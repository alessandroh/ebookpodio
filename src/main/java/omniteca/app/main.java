package omniteca.app;

import it.omniteca.collection.downloader.IcerboxDownload;
import it.omniteca.collection.finder.AvaxHomeStart;
import it.omniteca.navigation.browser.Browser;
import it.omniteca.navigation.download.IcerboxFind;
import it.omniteca.utility.database.Database;
import it.omniteca.utility.tool.Tool;
import it.omniteca.utility.varproperty.VarProperty;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import it.omniteca.podio.Podio;

public class main {

    /**
     * @param args the command line arguments
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException, Exception {
        Logger log = Logger.getLogger(main.class);
        String numPage = "-1";
        final String ICERBOX = "icerbox.com";

//------carico le variabili dal properties e le metto in un HashMap---------//
        VarProperty varP = new VarProperty();
        HashMap freeBookParams = new HashMap();
        HashMap dailyUpParams = new HashMap();
        HashMap avaxHomeParams = new HashMap();
        HashMap icerboxParams = new HashMap();
        HashMap generalParams = new HashMap();
        HashMap podioParams = new HashMap();

        try {
            generalParams = varP.loadGeneralParams();
            freeBookParams = varP.loadFreeBookParams();
            dailyUpParams = varP.loadDailyUpParams();
            avaxHomeParams = varP.loadAvaxHomeParams();
            icerboxParams = varP.loadIcerboxParams();
            podioParams = varP.loadPodioParams();
        } catch (IOException ex) {
            log.error(ex.getMessage());
            System.out.println("Impossibile trovare il file .config.properties oppure manca una proprietà");
            System.exit(0);
        }

        //------carico le cartelle che dovranno contenere i download -----------------//
        Tool tools = new Tool(generalParams);
        try {
            //crea la cartella per il download delle cover    
            tools.createCoverDir();
            //crea la cartella per il download di ebook
            tools.createDownloadEbookDir();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            System.out.println("Problemi con la creazione della cartella");
            System.exit(0);
        }

        //------leggo gli argomenti inseriti e salvo in un HashMap -------------------//
        HashMap argument = new HashMap();
         argument = tools.createHashArgs(args);
        //argument.put("c", "sync");

        try {
            //------1° caso - argomento FINDING ----------// 
            if (argument.get("c") != null && argument.get("c").equals("finding")) {
                //controllo l'esistenza dell' argomento numero pagina
                if (argument.get("bk") != null) {
                    numPage = argument.get("bk").toString();
                }

                try {
                    //------mi connetto al database-----------//
                    Database data = new Database(generalParams);
                    System.out.println("Database avviato con successo");

                    //------avvio un browser chrome----------//
                    Browser brsw = new Browser(generalParams);
                    WebDriver driver = brsw.startChromeBrowser();

                    //------mi collego al sito--------------//
                    if (argument.get("st") == null) {
                        System.out.println("inserire comando st-");
                        System.exit(0);
                    } else {
                        switch (argument.get("st").toString()) {
                            case "a":
                                AvaxHomeStart avax = new AvaxHomeStart(avaxHomeParams, generalParams);
                                avax.goToAvaxHome(driver, data, numPage);
                                break;
                            default:
                                System.out.println("comando st- nn valido consultare README");
                        }
                    }
                    //chiudo la connessione al database
                    data.closeConnection();
                    driver.close();
                    System.out.println("Programma terminato");
                } catch (SQLException ex) {
                    log.error(ex.getMessage());
                    switch (ex.getErrorCode()) {
                        case 1049:
                            System.out.println("Impossibile connettersi al database nome database errato nell'indirizzo host");
                            break;
                        case 1045:
                            System.out.println("Impossibile connettersi al database nome utente o password errate");
                            break;
                        case 0:
                            System.out.println("Impossibile connettersi al database controllare l'host nel link di comunicazione");
                            break;
                        case 1054:
                            System.out.println("Errore nella scrittura del record. Mancanza colonna o errore nella sintassi");
                            break;
                        case 1064:
                            System.out.println("Errore nella sintassi della query. Controllare presenza caratteri speciale");
                        default:
                            System.out.println("Errore nella classe database");
                            break;
                    }

                } catch (IllegalStateException ex) {
                    log.error(ex.getMessage());
                    System.out.println("Impossibile connettersi al browser. Controllare percorso del WebDriver");

                } catch (NoSuchElementException ex) {
                    log.error(ex.getMessage());
                    System.out.println("Impossibile caricare l'elemento, pagina o path sbagliati");
                } catch (NumberFormatException ex) {
                    log.error(ex.getMessage());
                    System.out.println("Inserisci nell'args un numero valido");
                }
                System.exit(0);
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            log.error(ex.getMessage());
            System.out.println("args mancante, inserire un argomento");
            System.exit(0);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            System.out.println("Altri tipi di errore, consultare log");
            System.exit(0);
        }

        //-----  2° caso  - argomento download ------//
        if (argument.get("c") != null && argument.get("c").equals("download")) {
            Browser brsw = new Browser(generalParams);
            Database data = new Database(generalParams);
            IcerboxDownload icerDown = new IcerboxDownload(icerboxParams, generalParams);
            IcerboxFind icerFind = new IcerboxFind(icerboxParams, generalParams);
            ResultSet rs;
            String status;
            String id;
            String link;
            String nameFile;
            String title;
            ArrayList<String> notDownloadeIds;
            notDownloadeIds = new ArrayList<>();
            boolean downComplete;
            WebDriver driver;

            try {
                // trovo tutti i record e li scarico
                rs = data.findBySitenameAndStatus(ICERBOX, Database.STATUS_NEW);
                //apro la finestra del browser
                driver = brsw.startChromeBrowserDownloadVersion();

                while (data.isNext(rs) == true) {
                    //salvo id 
                    id = data.getId(rs);
                    //link per il download
                    link = data.getLink(rs);
                    System.out.println("Trovato un link icerbox: " + link + " con id=" + id);
                    //scarico il file
                    icerDown.startDownload(driver, link);

                    if (icerFind.findBadLink(driver) == true) {
                        //inserisco status link corrotto
                        data.setStatus(Database.STATUS_CORRUPT, id);
                        //chiudo la finestra e riapro un nuovo browser
                        driver.close();
                        driver = brsw.startChromeBrowser();
                    } else {
                        //inserisco status come checkFile status=11
                        data.setStatus(Database.STATUS_CHECKFILE, id);
                        //chiudo la finestra e riapro un nuovo browser
                        Thread.sleep(5000);
                    }
                }
            } catch (InterruptedException | SQLException ex) {
                log.error(ex.getMessage());
                System.out.println("Eccezione rilevata");
            }
        }

        if (argument.get("c") != null && argument.get("c").equals("sync")) {
            Database data = new Database(generalParams);
            Podio pod = new Podio(podioParams, generalParams);
            try {
                //ricavo dal database i record che dovro salvare in podio
                ResultSet rs = data.findAllRecordsFirebase();
                if (rs.getRow() != 0) {
                    //creo l'ebook su Podio e aggiorno il database
                    pod.createEbookPodio(rs, data);
                }else{
                    System.out.println("Non hai elementi da sincronizzare");
                }

            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        } else {
            System.out.println("Argomento non valido controllare la guida");
        }
    }

}
