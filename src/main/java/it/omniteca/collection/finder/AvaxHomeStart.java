/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.collection.finder;

import it.omniteca.navigation.download.AvaxHomeFind;
import it.omniteca.navigation.download.Download;
import it.omniteca.utility.database.Database;
import it.omniteca.utility.tool.Tool;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author alex
 */
public class AvaxHomeStart {

    HashMap avaxHomeParams;
    HashMap generalParams;
    String address;
    String classDetail;
    String classDownFile;
    String classFullDetail;
    String downloadDir;
    Logger log = Logger.getLogger(AvaxHomeStart.class);

    public AvaxHomeStart(HashMap avaxHomeParams, HashMap generalParams) {
        this.avaxHomeParams = avaxHomeParams;
        this.generalParams = generalParams;
        this.address = avaxHomeParams.get("address").toString();
        this.classDetail = avaxHomeParams.get("classDetail").toString();
        this.classDownFile = avaxHomeParams.get("classDownFile").toString();
        this.classFullDetail = avaxHomeParams.get("classFullDetail").toString();
        this.downloadDir = generalParams.get("downloadDir").toString();
    }

    public void goToAvaxHome(WebDriver driver, Database data, String numPage) throws Exception {
        AvaxHomeFind avax = new AvaxHomeFind(avaxHomeParams);
        Download down = new Download(generalParams);
        Tool tools = new Tool(generalParams);
        WebElement detailLink;
        WebElement downLink;
        String linkToDownload;
        String nameDownloadFile;
        String coverName;
        boolean findCaptcha = false;
        boolean cond;
        boolean isMultiformat;
        boolean isDuplicate = false;
        boolean isStatusNew;
        boolean coverDownload;
        String fullDetails;
        String authorName;
        String title;
        String nameSite;
        int multiformat;
        String description;
        String ebookCover;
        int count = 0;
        int pageCount;
        ResultSet dataResult;
        ResultSet result;
        ResultSet cover;

        //controllo il numero pagina
        try {
            pageCount = Integer.parseInt(numPage);
        } catch (NumberFormatException ex) {
            throw ex;
        }

        //mi collego al sito
        try {
            driver.get(address);
            System.out.println("Accesso al sito AvaxHome eseguito");
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        do {
            //creo una lista con tutti gli elementi ebook di una pagina
            List<WebElement> list = avax.findEbookList(driver);
            //per ogni elemento clicco su detail
            for (int i = 0; i < list.size(); i++) {
                //trovo l'elemento link DETAIL
                detailLink = list.get(i);
                //clicco il link  
                detailLink.click();
                //trovo nome dell'autore e titolo del libro
                fullDetails = avax.getFullDetail(driver);
                //nome autore
                authorName = avax.nameAuthor(fullDetails);
                //titolo libro
                title = avax.nameTitle(fullDetails);
                //descrizione del libro
                description = avax.getDescription(driver);
                try {
                    //scarico cover
                    //trovo l'estensione del file che andro a scaricare
                    String extension = avax.getExtension(driver);
                    //nome del file immagine da scaricare
                    coverName = fullDetails.trim() + "." + extension;
                    //cerca nel database il record con il nome
                    cover = data.findByCoverName(coverName);
                    //controllo il numero dei record trovati andando alla fine del resultset
                    cover.last();
                    int numRow = cover.getRow();
                    //se non trovo elementi posso scaricarla
                    if (numRow == 0) {
                        //scarico l'immagine
                        down.downloadImage(driver, fullDetails.trim(), extension);
                        Thread.sleep(5000);
                        System.out.println("cover del libro scaricata");
                        coverDownload = true;
                    } else {
                        coverDownload = false;
                        System.out.println("cover scaricata in precedenza");
                    }
                } catch (IOException | InterruptedException | SQLException ex) {
                    throw ex;
                }
                //creo una lista con i link alla pagina del download
                List<WebElement> downList = avax.findDownloadList(driver);
                isMultiformat = avax.findMultiformat(downList);
                if (isMultiformat == true) {
                    multiformat = 1;
                } else {
                    multiformat = 0;
                }
                //trovo l'elemento download to ...
                downLink = downList.get(0);
                //clicco sul link
                downLink.click();
                //salvo l'indirizzo della pagina del download
                linkToDownload = avax.getLinkToDownload(driver);
                isStatusNew = true;
                try {
                    //se status e 9 e link uguale
                    dataResult = data.findByStatus(Database.STATUS_ON_FINDING);
                    while (dataResult.next()) {
                        String link = data.getLink(dataResult);
                        if (link.equals(linkToDownload)) {
                            System.out.println("link gia presente in database in attesa di conferma come nuovo link");
                            isStatusNew = false;
                            break;
                        }

                    }

                    if (data.isDuplicateLink(linkToDownload) == false) {
                        if (isStatusNew == true) {
                            System.out.println("Trovato un nuovo ebook: " + title);
                        } else {
                            System.out.println("Il libro: " + title + " è gia salvato ma non ancora confermato");
                        }
                        //salvo il nome del sito che contiene il file da scaricare
                        nameSite = avax.nameSiteDownload(linkToDownload);
                        try {
                            //attendo
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            log.error(ex.getMessage());
                            throw ex;
                        }
                        //salvo il nome del file da scaricare
                        nameDownloadFile = avax.nameDownloadFile(driver);
                        do {
                            //vado indietro di una pagina
                            driver.navigate().back();
                            try {
                                downList = avax.findDownloadList(driver);
                            } catch (Exception e) {
                                log.error(e.getMessage());
                                throw e;
                            }
                        } while (downList == null);

                        if (isStatusNew == true) {
                            data.insertRecordFromAvax(linkToDownload, nameDownloadFile, title, nameSite, multiformat, authorName, Database.STATUS_ON_FINDING, description);
                            System.out.println("Il libro " + title + " è stato inserito nel database");

                            if (coverDownload == true) {
                                ResultSet link = data.findByLink(linkToDownload);
                                while (link.next()) {
                                    String id = data.getId(link);
                                    data.updateCoverFromAvax(coverName, id);
                                }
                            }
                        }
                        //vado indietro di una pagina
                        driver.navigate().back();
                        //ricarico lista
                        list = avax.findEbookList(driver);
                        //non e duplicato
                        isDuplicate = false;
                    } else {
                        System.out.println("Trovato duplicato.");
                        isDuplicate = true;
                        break;
                    }

                } catch (Exception ex) {
                    log.error(ex.getMessage());
                    System.out.println("inserimento record fallito");
                    throw ex;
                }
            }
            //cambia pagina
            cond = avax.nextPage(driver);
            count++;
        } while (cond == true && isDuplicate == false && count != pageCount);

        //setto lo status dei record a 10
        result = data.findByStatus(Database.STATUS_ON_FINDING);
        while (result.next()) {
            String id = data.getId(result);
            data.setStatus(Database.STATUS_NEW, id);
            System.out.println("il file con id: " + id + " è stato settato correttamente con lo status 10 !");
        }
    }
}
