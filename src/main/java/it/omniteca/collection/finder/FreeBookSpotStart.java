/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.collection.finder;

import it.omniteca.navigation.download.FreeBookSpotFind;
import it.omniteca.navigation.menu.FreeBookSpotMenu;
import it.omniteca.utility.database.Database;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author alex
 */
public class FreeBookSpotStart {
    HashMap params;
    String downloadDir;
    String address;
    String numPage;
    
    public FreeBookSpotStart(HashMap freeBookParams, HashMap generalParams){
      this.params = freeBookParams;  
      this.address = freeBookParams.get("address").toString();
      this.downloadDir = generalParams.get("downloadDir").toString();
      this.numPage = freeBookParams.get("nrPage").toString();
    }
    public void gotoFreeBookSpot(WebDriver driver,Database data) throws SQLException, Exception {
        List<WebElement> listTable;
        WebElement e;
        String link;
        String title;
        String nameSite;
        boolean cond = false;
        int count = 0;
        int nrPage = Integer.parseInt(numPage);
        //Database data = new Database(params);
        FreeBookSpotMenu menu = new FreeBookSpotMenu(params);
        FreeBookSpotFind fnd = new FreeBookSpotFind(params);
        //mi collego al sito
        driver.get(address);
        //seleziono gli ebook italiani
        menu.selectionDropdownMenu(driver);
        do {
            //inserisco in una table l'elemento ebook
            listTable = fnd.findLinkElement(driver);
            //scorro la lista 
            for (int i = 0; i < listTable.size(); i++) {
                //copio l'elemento
                e = listTable.get(i);
                title = fnd.findTitle(e);
                //clicco sul link
                e.click();
                //salvo l'elemento link diretto
                e = fnd.linkToDownload(driver);
                //salva una String del link
                link = fnd.toString(e);
                //salvo il nome del sito
                nameSite = fnd.findSiteName(link);
                //controlla se è duplicato
                if (data.isDuplicateLink(link) == false) {
                    //salvo l'immagine dell'ebook
                    fnd.saveImage(driver, title,downloadDir);
                    //inserisce nel database
                    //data.insertRecord(link, title, nameSite);
                }
                //torno alla pagina precedente
                driver.navigate().back();
                //ricarico elementi
                listTable = fnd.findLinkElement(driver);
            }
            count++;
            if (count < nrPage) {
                //controlla condizione e clicca su pagina avanti
                cond = fnd.nextPage(driver);
            } else {
                cond = false;
            }
        } while (cond == true);
    }
}
