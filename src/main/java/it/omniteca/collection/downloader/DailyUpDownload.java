/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.collection.downloader;

import it.omniteca.navigation.download.DailyUpFind;
import java.util.Hashtable;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author alex
 */
public class DailyUpDownload {

    Hashtable params;

    public DailyUpDownload(Hashtable params) {
        this.params = params;
    }

    public String downloadLinkDailyup(WebDriver driver, String link) {
        DailyUpFind daily = new DailyUpFind(params);
        String downloadLink;
        //mi collego al link
        driver.get(link);
        //seleziono free download
        daily.selectFreeDownload(driver);
        //tolgo la spunta
        daily.deleteSign(driver);
        //clicco su download
        daily.clickButtonDownload(driver);
        //link diretto per il download
        downloadLink = daily.startDownloadLink(driver);

        return downloadLink;
    }
    
    
}
