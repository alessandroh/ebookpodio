/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.collection.downloader;

import it.omniteca.navigation.browser.Browser;
import it.omniteca.navigation.download.IcerboxFind;
import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author alex
 */
public class IcerboxDownload {

    HashMap icerboxParams;
    HashMap generalParams;
    String downloadButton;

    public IcerboxDownload(HashMap icerboxParams, HashMap generalParams) {
        this.icerboxParams = icerboxParams;
        this.generalParams = generalParams;
        this.downloadButton = icerboxParams.get("downloadButton").toString();
    }

    public void startDownload(WebDriver driver, String link) throws Exception {
        IcerboxFind icerFind = new IcerboxFind(icerboxParams, generalParams);
        Browser browser = new Browser(generalParams);
        boolean findLogin;
        try {
            //mi collego al link
            driver.get(link);
            System.out.println("--- inizio il download ---");
            //controllo se e presente captcha
            if (icerFind.findCaptcha(driver) == true) {
                System.out.println("trovato captcha...attendo il riconoscimento");
                icerFind.waitForClassname(driver);
            }
            //controllo se sono loggato
            findLogin = icerFind.findLogin(driver);
            if (findLogin == true) {
                System.out.println("non sei un utente loggato");
                //effettuo il login
                icerFind.loginIcerbox(driver);
                //driver.close();
                //driver = browser.startChromeBrowser();
                driver.get(link);
            }
            if (icerFind.findBadLink(driver) == true) {
                System.out.println("link corrotto");
                throw new Exception("link corrotto o inesistente");
            }
            if (icerFind.findDownloadButton(driver) == true) {
                driver.findElement(By.xpath(downloadButton)).click();
            }

        } catch (Exception ex) {
            throw ex;
        }
    }
}
