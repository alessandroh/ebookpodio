/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.podio;

import com.podio.APIFactory;
import com.podio.ResourceFactory;
import com.podio.file.FileAPI;
import com.podio.item.FieldValuesUpdate;
import com.podio.item.ItemAPI;
import com.podio.item.ItemCreate;
import com.podio.oauth.OAuthClientCredentials;
import com.podio.oauth.OAuthUsernameCredentials;
import it.omniteca.utility.database.Database;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author alex
 */
public class Podio {
    
    public static final int STATUS_NEW_ENTRY = 1;

    APIFactory apiFactory;
    String idClient;
    String secretKey;
    String emailPodio;
    String passwPodio;
    String coverPath;
    int appId;
    Logger log = Logger.getLogger(Podio.class);

    public Podio(HashMap podioParams, HashMap generalParams) {
        this.idClient = podioParams.get("idClient").toString();
        this.secretKey = podioParams.get("secretKey").toString();
        this.emailPodio = podioParams.get("emailPodio").toString();
        this.passwPodio = podioParams.get("passwPodio").toString();
        this.coverPath = generalParams.get("downloadDir").toString();
        this.appId = Integer.parseInt(podioParams.get("appId").toString());

        ResourceFactory resourceFactory = new ResourceFactory(
                new OAuthClientCredentials(idClient, secretKey),
                new OAuthUsernameCredentials(emailPodio, passwPodio));
        this.apiFactory = new APIFactory(resourceFactory);
    }

    public void createEbookPodio(ResultSet rs, Database data) throws SQLException {
        String author,title,id,description,coverName;        
        int numId;
        ItemAPI itemAPI = apiFactory.getItemAPI();
        ItemCreate item;

        try {
            while (rs.next()) {
                List<FieldValuesUpdate> list = new ArrayList<>();

                author = data.getAuthor(rs);
                title = data.getTitle(rs);
                id = (data.getId(rs));
                description = data.getDescription(rs);
                coverName = data.getCoverName(rs);
                numId = saveEbookCover(coverName);

                list.add(new FieldValuesUpdate("autore", setFieldValue(author).getValues()));
                list.add(new FieldValuesUpdate("titolo", setFieldValue(title).getValues()));
                list.add(new FieldValuesUpdate("testo", setFieldValue(description).getValues()));
                list.add(new FieldValuesUpdate("stato", setFieldValueNum(STATUS_NEW_ENTRY).getValues()));
                list.add(new FieldValuesUpdate("copertina", setFieldValueNum(numId).getValues()));

                item = new ItemCreate("externalId", list, Collections.<Integer>emptyList(), Collections.<String>emptyList());
                itemAPI.addItem(appId, item, false);
                data.setSync(id, Database.STATUS_SYNC);
            }

        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

    }

    public FieldValuesUpdate setFieldValue(String val) {

        FieldValuesUpdate fieldValue = new FieldValuesUpdate();
        List test = new ArrayList();
        Map map = new HashMap();
        map.put("value", val);
        test.add(map);
        fieldValue.setValues(test);

        return fieldValue;
    }

    public FieldValuesUpdate setFieldValueNum(int num) {
        FieldValuesUpdate fieldValue = new FieldValuesUpdate();
        List test = new ArrayList();
        Map map = new HashMap();
        map.put("value", num);
        test.add(map);
        fieldValue.setValues(test);

        return fieldValue;
    }

    /**
     * recupera l'id dell'immagine dopo averla caricata nel database di Podio
     *
     * @param coverFilename
     * @return
     */
    public int saveEbookCover(String coverFilename) {
        File file = new File(coverPath + coverFilename);
        FileAPI fileAPI = apiFactory.getFileAPI();
        int fileId = fileAPI.uploadFile(coverFilename, file);
        return fileId;
    }

}
