/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.navigation.browser;

import java.util.HashMap;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author alex
 */
public class Browser {

    String webDriverProp;
    String webDriverPath;
    String torHost;
    String torPort;
    String pathProfile;
    String downloadDir;
    String ebookDownloadDir;
    
    Logger log = Logger.getLogger(Browser.class);

    public Browser(HashMap params) {
        this.webDriverProp = params.get("webDProp").toString();
        this.webDriverPath = params.get("webDPath").toString();
        this.torHost = params.get("torHost").toString();
        this.torPort = params.get("torPort").toString();
        this.pathProfile = params.get("pathProfile").toString();
        this.downloadDir = params.get("downloadDir").toString();
        this.ebookDownloadDir = params.get("ebookDownloadDir").toString();
    }

    /**
     * apre un browser chrome
     *
     * @return
     */
    public WebDriver startChromeBrowser() {
        System.setProperty(webDriverProp, webDriverPath);
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", downloadDir);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("user-data-dir=/" + pathProfile);
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(cap);
        return driver;
    }

    public WebDriver startChromeBrowserDownloadVersion() {
        System.setProperty(webDriverProp, webDriverPath);
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", ebookDownloadDir);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("user-data-dir=/" + pathProfile);
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(cap);
        return driver;
    }

    /**
     * apre un browser anonimo
     *
     * @return
     */
    public WebDriver startTorBrowser() {
        WebDriver driver = null;
        try {
            System.setProperty(webDriverProp, webDriverPath);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--proxy-server=socks5://" + torHost + ":" + torPort);
            options.addArguments("user-data-dir=/" + pathProfile);
            driver = new ChromeDriver(options);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return driver;
    }
}
