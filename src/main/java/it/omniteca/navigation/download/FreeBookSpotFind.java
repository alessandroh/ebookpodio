/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.navigation.download;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author alex
 */
public class FreeBookSpotFind {

    String srcElementPath;
    String downloadLinkPath;
    String nextPagePath;
    String srcImageId;
    
    Logger log = Logger.getLogger(FreeBookSpotFind.class);
    
    public FreeBookSpotFind(HashMap params) {
        this.srcElementPath = params.get("srcElementPath").toString();
        this.downloadLinkPath = params.get("downloadLinkPath").toString();
        this.nextPagePath = params.get("nextPagePath").toString();
        this.srcImageId = params.get("srcImageId").toString();
    }

    /**
     * trova l'elemento ebook e lo salva in una List
     *
     * @param driver
     * @return
     */
    public List<WebElement> findLinkElement(WebDriver driver) {
        List<WebElement> table = driver.findElements(By.xpath(srcElementPath));

        return table;
    }

    /**
     * trova l'elemento con il link diretto al download
     *
     * @param driver
     * @return
     */
    public WebElement linkToDownload(WebDriver driver) {
        WebElement element;
        element = driver.findElement(By.xpath(downloadLinkPath));

        return element;
    }

    /**
     * trova l'elemento pagina avanti e clicca
     *
     * @param driver
     * @return
     */
    public boolean nextPage(WebDriver driver) {
        WebElement e;
        boolean cond = true;
        e = driver.findElement(By.xpath(nextPagePath));
        if (e != null) {
            e.click();
        } else {
            cond = false;
        }
        return cond;
    }

    /**
     * formatta l'elemento link in una Stringa
     *
     * @param element
     * @return
     */
    public String toString(WebElement element) {
        String a = element.getText();
        String b = a.substring(0, a.length() - 3);

        return b;
    }

    /**
     * trova il titolo dell'ebook nel sito FreeBookSpot
     *
     * @param element
     * @return
     */
    public String findTitle(WebElement element) {
        String title = element.getText();
        if (title.contains("[Italian]") || title.contains("(Italian Edition)")) {
            title = title.replace("[Italian]", "").replace("(Italian Edition)", "");
        }
        return title;
    }
    
    /**
     * trovo il nome del sito dove è presente il file da scaricare
     * @param link
     * @return 
     */
    public String findSiteName (String link){
        String nameSite;
        nameSite = link.split("//")[1].split("/")[0];
        
        return nameSite;
    }
    
    /**
     * salva le immagini dal sito FreeBookSpot
     * @param driver
     * @param nameFile
     * @param downloadDir
     */
    public void saveImage(WebDriver driver, String nameFile, String downloadDir) {

        new File(downloadDir).mkdirs();
        try {
            WebElement logo = driver.findElement(By.id(srcImageId));
            String logoSRC = logo.getAttribute("src");

            URL imageURL = new URL(logoSRC);
            BufferedImage saveImage = ImageIO.read(imageURL);

            ImageIO.write(saveImage, "png", new File(downloadDir + nameFile + ".png"));

        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }
}
