/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.navigation.download;

import java.io.File;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author alex
 */
public class IcerboxFind {

    HashMap icerboxParams;
    HashMap generalParams;
    String findLoginPath;
    String emailForm;
    String passwordForm;
    String icerboxEmail;
    String icerboxPassw;
    String logButton;
    String ebookDownloadDir;
    String captchaIcerboxPath;
    String downloadButton;
    String badLinkPath;

    Logger log = Logger.getLogger(IcerboxFind.class);

    public IcerboxFind(HashMap icerboxParams, HashMap generalParams) {
        this.icerboxParams = icerboxParams;
        this.generalParams = generalParams;
        this.findLoginPath = icerboxParams.get("findLoginPath").toString();
        this.emailForm = icerboxParams.get("emailForm").toString();
        this.passwordForm = icerboxParams.get("passwordForm").toString();
        this.icerboxEmail = icerboxParams.get("icerboxEmail").toString();
        this.icerboxPassw = icerboxParams.get("icerboxPassw").toString();
        this.logButton = icerboxParams.get("logButton").toString();
        this.ebookDownloadDir = generalParams.get("ebookDownloadDir").toString();
        this.captchaIcerboxPath = icerboxParams.get("captchaIcerboxPath").toString();
        this.downloadButton = icerboxParams.get("downloadButton").toString();
        this.badLinkPath = icerboxParams.get("badLinkPath").toString();
    }

    public String fileName(String link) {
        String fileName;
        String[] splitter;
        int splitSize;

        splitter = link.split("/");
        fileName = splitter[(splitter.length) - 1].trim();
        
        return fileName;
    }

    public boolean findLogin(WebDriver driver) {
        WebElement element;
        boolean findIt = true;

        try {
            element = driver.findElement(By.xpath(findLoginPath));
        } catch (NoSuchElementException ex) {
            findIt = false;
        }
        return findIt;
    }

    public void loginIcerbox(WebDriver driver) throws InterruptedException {
        WebElement element;
        System.out.println("Effettuo il login");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            log.error(ex);
            throw ex;
        }
        //trovo l'elemento login
        element = driver.findElement(By.xpath(findLoginPath));
        element.click();
        //trovo l'elemento email
        element = driver.findElement(By.xpath(emailForm));
        element.sendKeys(icerboxEmail);
        //trovo l'elemento password
        element = driver.findElement(By.xpath(passwordForm));
        element.sendKeys(icerboxPassw);
        //clicco sul login
        element = driver.findElement(By.xpath(logButton));
        element.submit();
        Thread.sleep(4000);
        System.out.println("Login effettuato");
    }

    public boolean findEbook(String nameFile) throws InterruptedException {
        boolean scaricato = false;
        String secondNameFile;
        String[] list;

        //leggi il contenuto della cartella       
        File ebook = null;
        try {
            ebook = new File(ebookDownloadDir);
        } catch (Exception e) {
            log.fatal(e);
            throw e;
        }
        //System.out.println("Controllo il download del libro " + nameFile);
        secondNameFile = nameFile.replaceFirst("\\.", " ");

        //crea una lista degli elementi
        list = ebook.list();
        if (list.length > 0) {
            for (String list1 : list) {
                //controllo se il file e scaricato
                if (list1.equals(nameFile) || list1.equals(secondNameFile)) {
                    scaricato = true;
                    break;
                }
            }
        }

        return scaricato;
    }

    public boolean findCaptcha(WebDriver driver) {
        WebElement element;
        boolean findIt = true;

        try {
            element = driver.findElement(By.xpath(captchaIcerboxPath));
        } catch (NoSuchElementException ex) {
            findIt = false;
        }
        return findIt;
    }

    public void waitForClassname(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 150);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(findLoginPath)));
    }

    public boolean findDownloadButton(WebDriver driver) {
        WebElement element;
        boolean findIt = true;

        try {
            element = driver.findElement(By.xpath(downloadButton));
        } catch (NoSuchElementException ex) {
            findIt = false;
        }
        return findIt;
    }

    public boolean findBadLink(WebDriver driver) {
        WebElement element;
        boolean findIt = true;

        try {
            element = driver.findElement(By.xpath(badLinkPath));
        } catch (Exception ex) {
            findIt = false;
        }
        return findIt;
    }
}
