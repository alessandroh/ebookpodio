/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.navigation.download;

import java.util.Hashtable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author alex
 */
public class DailyUpFind {

    String freeDownloadPath;
    String delSign;
    String downButton;
    String startDownLink;

    public DailyUpFind(Hashtable params) {
        this.freeDownloadPath = params.get("freeDownloadPath").toString();
        this.delSign = params.get("delSign").toString();
        this.downButton = params.get("downButton").toString();
        this.startDownLink = params.get("startDownLink").toString();
    }

    /**
     * seleziono free download
     *
     * @param driver
     */
    public void selectFreeDownload(WebDriver driver) {
        WebElement element;
        element = driver.findElement(By.xpath(freeDownloadPath));
        element.click();
    }

    /**
     * tolgo la spunta
     *
     * @param driver
     */
    public void deleteSign(WebDriver driver) {
        WebElement sign;
        sign = driver.findElement(By.id(delSign));
        sign.click();
    }

    /**
     * clicco sul bottone per il download
     *
     * @param driver
     */
    public void clickButtonDownload(WebDriver driver) {
        WebElement button;
        button = driver.findElement(By.id(downButton));
        button.click();
    }

    /**
     * trova il link diretto al download
     *
     * @param driver
     * @return
     */
    public String startDownloadLink(WebDriver driver) {
        WebElement start;
        String startLink;
        start = driver.findElement(By.xpath(startDownLink));
        startLink = start.getAttribute("href");

        return startLink;
    }

    /**
     * ricavo il nome del file dal link
     *
     * @param url
     * @return
     */
    public String getNameFile(String url) {
        String nameFile;
        String[] splitter;
        splitter = url.split("/");
        nameFile = splitter[splitter.length - 1];

        return nameFile;
    }
}
