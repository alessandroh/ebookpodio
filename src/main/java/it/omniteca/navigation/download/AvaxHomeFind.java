/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.navigation.download;

import java.util.HashMap;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 *
 * @author alex
 */
public class AvaxHomeFind {

    String classDetail;
    String classTitle;
    String downPath;
    String classDownFile;
    String classCaptcha;
    String classFullDetail;
    String classNextPage;

    Logger log = Logger.getLogger(AvaxHomeFind.class);

    public AvaxHomeFind(HashMap avaxHomeParams) {
        this.classDetail = avaxHomeParams.get("classDetail").toString();
        this.classTitle = avaxHomeParams.get("classTitle").toString();
        this.downPath = avaxHomeParams.get("downPath").toString();
        this.classDownFile = avaxHomeParams.get("classDownFile").toString();
        this.classCaptcha = avaxHomeParams.get("classCaptcha").toString();
        this.classFullDetail = avaxHomeParams.get("classFullDetail").toString();
        this.classNextPage = avaxHomeParams.get("classNextPage").toString();
    }

    /**
     * trova la lista di ebook di ogni pagina
     *
     * @param driver
     * @return
     */
    public List<WebElement> findEbookList(WebDriver driver) {
        List<WebElement> list;
        list = driver.findElements(By.className(classDetail));
        return list;
    }

    /**
     * trova nome dell'autore e titolo del libro non formattato
     *
     * @param driver
     * @return
     */
    public String getFullDetail(WebDriver driver) {
        WebElement el;
        String name;
        el = driver.findElement(By.xpath(classFullDetail));
        name = el.getText();
        while (name == null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                log.error(ex.getMessage());
            }
        }
        name = name.replaceAll("\\/", "\\\\");
        return name;
    }

    /**
     * trova la lista dei link alla pagina di download
     *
     * @param driver
     * @return
     */
    public List<WebElement> findDownloadList(WebDriver driver) {
        List<WebElement> list;
        list = driver.findElements(By.xpath(downPath));
        return list;
    }

    /**
     * link diretto per il download
     *
     * @param driver
     * @return
     */
    public String getLinkToDownload(WebDriver driver) {
        String link;
        link = driver.getCurrentUrl();
        while (link == null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                log.error(ex.getMessage());
            }
        }
        return link;
    }

    /**
     * nome del file memorizzato
     *
     * @param driver
     * @return
     */
    public String nameDownloadFile(WebDriver driver) {
        String nameFile;
        WebElement element;

        element = driver.findElement(By.className(classDownFile));
        nameFile = element.getText();
        while (nameFile == null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                log.error(ex.getMessage());
            }
        }
        if (nameFile.contains("[")) {
            nameFile = nameFile.split("\\[")[0].trim();
        }
        return nameFile;
    }

    /**
     * controllo se è presente la pagina del captcha
     *
     * @param driver
     * @return
     */
    public boolean findCaptcha(WebDriver driver) {
        WebElement captcha;
        boolean findIt = false;

        captcha = driver.findElement(By.className(classCaptcha));
        if (captcha == null) {
            throw new NoSuchElementException("no such element");
        } else {
            findIt = true;
        }

        return findIt;
    }

    /**
     * attendo la prima verifica captcha
     *
     * @param driver
     * @param xpath
     */
    public void waitForClassname(WebDriver driver, String xpath) {
        WebDriverWait wait = new WebDriverWait(driver, 150);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(xpath)));
    }

    /**
     * scorre le pagine
     *
     * @param driver
     * @return
     */
    public boolean nextPage(WebDriver driver) {
        WebElement e;
        boolean cond = false;
        try {
            e = driver.findElement(By.className(classNextPage));
            if (e != null) {
                e.click();
                cond = true;
            }

        } catch (NoSuchElementException ex) {
            
            cond=false;
        }

        return cond;
    }

    public String nameAuthor(String text) {
        text = text.replaceAll("\'", " ");
        String author;

        if (text.contains("-")) {
            author = text.split("-")[0].trim();
        } else {

            author = "none";
        }

        return author;
    }

    public String nameTitle(String text) {
        text = text.replaceAll("\'", " ");
        String title;

        if (text.contains("-")) {
            title = text.split("-")[1].trim();
        } else {
            title = text;
        }

        return title;
    }

    public String nameSiteDownload(String linkToDownload) {
        String nameSite;
        String[] list;

        list = linkToDownload.split("/");
        nameSite = list[2];

        return nameSite;
    }

    public boolean findMultiformat(List list) {
        int nrFormat;
        boolean isMultiformat = false;

        nrFormat = list.size();
        if (nrFormat > 1) {
            isMultiformat = true;
        }

        return isMultiformat;
    }
    
    public String getDescription(WebDriver driver){
        WebElement element;
        String fullText;
        try{
        element = driver.findElement(By.className("text"));
        fullText = element.getText().split("\n")[3].trim();
       // fullText = fullText.replaceAll("'", "\\\\'");
        }catch(Exception e){
          fullText ="";  
        }
        return fullText;
    }
    
    public String getExtension (WebDriver driver){
        WebElement image;
        String logoSRC;
        String extension;
        
        image = driver.findElement(By.className("img-responsive"));
        logoSRC = image.getAttribute("src");
        extension =   FilenameUtils.getExtension(logoSRC);
        
        return extension;
    }
}
