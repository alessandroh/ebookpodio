/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.navigation.download;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author alex
 */
public class Download {

    String ebookDownloadDir;
    String downloadDir;
    Logger log = Logger.getLogger(Download.class);

    public Download(HashMap params) {

        this.ebookDownloadDir = params.get("ebookDownloadDir").toString();
        this.downloadDir = params.get("downloadDir").toString();

    }

    /**
     * download del file
     *
     * @param driver
     * @param nameImage
     * @param extension
     * @throws java.net.MalformedURLException
     * @throws java.io.IOException
     */
    public void downloadImage(WebDriver driver, String nameImage, String extension) throws MalformedURLException, IOException {

        try {
            WebElement image;

            image = driver.findElement(By.className("img-responsive"));
            String logoSRC = image.getAttribute("src");

            URL imageURL = new URL(logoSRC);
            BufferedImage saveImage = ImageIO.read(imageURL);

            ImageIO.write(saveImage, extension, new File(downloadDir + nameImage + "." + extension));
        } catch (MalformedURLException ex) {
            log.info(ex.getMessage());
            throw ex;
        } catch (FileNotFoundException ex) {
            log.warn(ex.getMessage());
            System.out.println("Impossibile creare il file, controllare nome file");
            throw ex;
        } catch (IOException ex) {
            log.info(ex.getMessage());
            throw ex;
        }
    }

    public void downloadUsingStream(String urlStr, String file) throws MalformedURLException, FileNotFoundException, IOException {
        try {
            new File(ebookDownloadDir).mkdirs();
            URL url = new URL(urlStr);
            try (BufferedInputStream bis = new BufferedInputStream(url.openStream()); FileOutputStream fis = new FileOutputStream(ebookDownloadDir + file)) {
                byte[] buffer = new byte[1024];
                int count;
                while ((count = bis.read(buffer, 0, 1024)) != -1) {
                    fis.write(buffer, 0, count);
                }
            }
        } catch (MalformedURLException ex) {
            log.info(ex.getMessage());
            throw ex;
        } catch (FileNotFoundException ex) {
            log.info(ex.getMessage());
            throw ex;
        } catch (IOException ex) {
            log.info(ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            log.info(ex.getMessage());
            throw ex;
        }
    }

}
