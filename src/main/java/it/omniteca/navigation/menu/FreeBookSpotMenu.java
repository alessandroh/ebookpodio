/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.navigation.menu;

import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author alex
 */
public class FreeBookSpotMenu {
    String value;
    String dropId; 
    String dropSelectPath;
    
    public FreeBookSpotMenu (HashMap params){
        this.value = params.get("value").toString();
        this.dropId = params.get("dropId").toString();
        this.dropSelectPath = params.get("dropSelectPath").toString();
    }
    
    /**
     * seleziona la lingua italiana dal menu
     * @param driver 
     */
    public void selectionDropdownMenu(WebDriver driver) {
        Select dropdown = new Select(driver.findElement(By.id(dropId)));
        dropdown.selectByValue(value);
        driver.findElement(By.xpath(dropSelectPath)).click();
    }
}
