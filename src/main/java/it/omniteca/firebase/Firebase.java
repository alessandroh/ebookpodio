/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import org.apache.log4j.Logger;

/**
 *
 * @author alex
 */
public class Firebase {

    Logger log = Logger.getLogger(Firebase.class);

    public Firebase() throws FileNotFoundException, IOException, InterruptedException, ExecutionException {
        FileInputStream serviceAccount
                = new FileInputStream("/home/alex/SDK-FIREBASE-PERSONALE/appomniteca-firebase-adminsdk-w766z-b0204da9ce.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://appomniteca.firebaseio.com")
                .build();

        FirebaseApp myApp = FirebaseApp.initializeApp(options);

        FirebaseAuth defaultAuth = FirebaseAuth.getInstance(myApp);
        FirebaseDatabase defaultDatabase = FirebaseDatabase.getInstance(myApp);
        String uid = "vKqVMk2Au3f4DUM19AhNlOqIBtW2";
        //UserRecord userRecord = FirebaseAuth.getInstance().getUserAsync(uid).get();
        //See the UserRecord reference doc for the contents of userRecord.
        //System.out.println("Successfully fetched user data: " + userRecord.getUid());
        //System.out.println("Successfully fetched user data: " + userRecord.getEmail());

        
     
       // DatabaseReference ref = defaultDatabase.getReference("user");

  

    }

    public void createUser() {
        CreateRequest request = new CreateRequest()
                .setEmail("giovanni@nicolazzo.it")
                .setEmailVerified(false)
                .setPassword("omnit3ca")
                .setDisplayName("Giovanni Nicolazzo")
                .setDisabled(false);

        UserRecord userRecord;
        try {
            userRecord = FirebaseAuth.getInstance().createUserAsync(request).get();
            System.out.println("Successfully created new user: " + userRecord.getUid());
        } catch (InterruptedException | ExecutionException ex) {
            log.error(ex.getMessage());
        }

    }

}
