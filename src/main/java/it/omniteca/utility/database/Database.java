/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.utility.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author alex
 */
public class Database {

    public static final int STATUS_NEW = 10;
    public static final int STATUS_CHECKFILE = 11;
    public static final int STATUS_CORRUPT = 15;
    public static final int STATUS_COMPLETE = 20;
    public static final int STATUS_ON_FINDING = 9;
    public static final int STATUS_SYNC = 1;

    String host;
    String user;
    String pass;
    Connection connect;

    Logger log = Logger.getLogger(Database.class);

    /**
     * Classe Database - Ad ogni oggetto creato di questa classe sara avviata
     * una connessione al database.
     *
     * @param params hashtable con le variabili dei properties
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Database(HashMap params) throws ClassNotFoundException, SQLException {
        this.host = params.get("host").toString();
        this.user = params.get("user").toString();
        this.pass = params.get("pass").toString();
        Connection c = startDatabase();
        this.connect = c;
    }

    /**
     * connessione al database
     *
     * @return connessione
     */
    private Connection startDatabase() throws ClassNotFoundException, SQLException {

        Connection c = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection(host, user, pass);
            c.setAutoCommit(true);
        } catch (ClassNotFoundException | SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return c;
    }

    /**
     * inserisce un record nel database
     *
     * @param ebook_link
     * @param filename
     * @param ebook_title
     * @param siteName
     * @param multiformat
     * @param author
     * @param status
     * @param description
     * @throws SQLException
     */
    public void insertRecordFromAvax(String ebook_link, String filename, String ebook_title, String siteName, int multiformat, String author, Integer status, String description) throws SQLException {
        try {
            PreparedStatement stm;
            String sql = "INSERT INTO ebooks( ebook_link,ebook_title, status,description,site_name,multiformat,author,is_sync) VALUES(?,?,?,?,?,?,?,?);";
            stm = connect.prepareStatement(sql);

            stm.setString(1, ebook_link);
            stm.setString(2, ebook_title);
            stm.setInt(3, status);
            stm.setString(4, description);
            stm.setString(5, siteName);
            stm.setInt(6, multiformat);
            stm.setString(7, author);
            stm.setInt(8, 0);

            stm.executeUpdate();
            System.out.println("Record inserito");
            stm.close();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            //Logger.getLogger(Database.class.getName()).log(Level.SEVERE,null,ex);
            throw ex;
        }
    }
//

    /**
     * aggiorna il record dell'id passante, aggiungendo il nome della copertina
     * del scaricata
     *
     * @param coverName
     * @param id
     * @throws SQLException
     */
    public void updateCoverFromAvax(String coverName, String id) throws SQLException {
        try {
            PreparedStatement stmt;
            String sql = "UPDATE ebooks SET cover_name= ? WHERE ID= ?;";
            stmt = connect.prepareStatement(sql);
            stmt.setString(1, coverName);
            stmt.setString(2, id);
            stmt.executeUpdate();
            System.out.println("Record aggiornato con nome della cover");
            stmt.close();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            //Logger.getLogger(Database.class.getName()).log(Level.SEVERE,null,ex);
            throw ex;
        }
    }

    /**
     * trova l'elemento del database per il link al download
     *
     * @param link
     * @return ResultSet
     * @throws SQLException
     */
    public ResultSet findByLink(String link) throws SQLException {
        ResultSet rs = null;
        PreparedStatement stmt;
        try {
            String sql = "SELECT * FROM ebooks WHERE ebook_link = ?";
            stmt = connect.prepareStatement(sql);
            stmt.setString(1, link);
            rs = stmt.executeQuery();
            //stmt.close();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            //Logger.getLogger(Database.class.getName()).log(Level.SEVERE,null,ex);
            throw ex;
        }

        return rs;
    }

    public ResultSet findById(int id) throws SQLException {
        ResultSet rs = null;
        PreparedStatement stmt;
        try {
            String sql = "SELECT * FROM ebooks WHERE id = ?";
            stmt = connect.prepareStatement(sql);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            stmt.close();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return rs;
    }


    /*--- Elementi del database -----*/
    /**
     * trovo l'id del link da scaricare
     *
     * @param rs
     * @return
     * @throws java.sql.SQLException
     */
    public String getId(ResultSet rs) throws SQLException {
        String id = null;
        try {
            id = rs.getString("id");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return id;
    }

    /**
     * trovo l'autore
     *
     * @param rs
     * @return
     * @throws java.sql.SQLException
     */
    public String getAuthor(ResultSet rs) throws SQLException {
        String author = null;
        try {
            author = rs.getString("author");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return author;
    }

    /**
     * trovo covername
     *
     * @param rs
     * @return
     * @throws java.sql.SQLException
     */
    public String getCoverName(ResultSet rs) throws SQLException {
        String coverName = null;
        try {
            coverName = rs.getString("cover_name");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return coverName;
    }

    /**
     * trova il link del download
     *
     * @param rs
     * @return
     * @throws java.sql.SQLException
     */
    public String getLink(ResultSet rs) throws SQLException {
        String link = null;
        try {
            link = rs.getString("ebook_link");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return link;
    }

    /**
     * trova lo status del file
     *
     * @param rs
     * @return
     * @throws java.sql.SQLException
     */
    public String getStatus(ResultSet rs) throws SQLException {
        String status = null;
        try {
            status = rs.getString("status");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return status;
    }

    /**
     * trovo titolo ebook
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    public String getEbookTitle(ResultSet rs) throws SQLException {
        String ebookTitle = null;
        try {
            ebookTitle = rs.getString("ebook_title");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            //Logger.getLogger(Database.class.getName()).log(Level.SEVERE,null,ex);
            throw ex;
        }
        return ebookTitle;
    }

    /**
     * trova filename
     *
     * @param rs
     * @return
     * @throws java.sql.SQLException
     */
    public String getFilename(ResultSet rs) throws SQLException {
        String filename = null;
        try {
            filename = rs.getString("filename");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return filename;
    }

    public String getCopyreader_id(ResultSet rs) throws SQLException {
        String copy_id = null;
        try {
            copy_id = rs.getString("copyreader_id");
            if (copy_id == null) {
                copy_id = "0";
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return copy_id;
    }

    public String getMultiformat(ResultSet rs) throws SQLException {
        String multiformat = null;
        try {
            multiformat = rs.getString("multiformat");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return multiformat;
    }

    public String getCreated(ResultSet rs) throws SQLException {
        String created = null;
        try {
            created = rs.getString("created_at");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return created;
    }

    public String getUpdate(ResultSet rs) throws SQLException {
        String updated = null;
        try {
            updated = rs.getString("updated_at");
            if (updated == null) {
                updated = "0";
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return updated;
    }

    public String getSitename(ResultSet rs) throws SQLException {
        String sitename = null;
        try {
            sitename = rs.getString("site_name");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return sitename;
    }

    public String getDescription(ResultSet rs) throws SQLException {
        String description = null;
        try {
            description = rs.getString("description");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return description;
    }

    public String getSync(ResultSet rs) throws SQLException {
        String sync = null;
        try {
            sync = rs.getString("is_sync");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return sync;
    }

    /**
     * controlla se il link è un duplicato
     *
     * @param link
     * @return
     * @throws java.lang.Exception
     */
    public boolean isDuplicateLink(String link) throws Exception {
        boolean cond = false;
        ResultSet rs;
        PreparedStatement stmt;
        try {
            String sql = "SELECT ebook_link FROM ebooks WHERE status= ? ";
            stmt = connect.prepareStatement(sql);
            stmt.setInt(1, STATUS_NEW);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String url = rs.getString("ebook_link");
                if (url.equals(link)) {
                    cond = true;
                    break;
                }
            }
            stmt.close();
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw e;
        }
        return cond;
    }

    /**
     * cerca nel database il nome del file della cover scaricata
     *
     * @param colName
     * @return
     * @throws SQLException
     */
    public ResultSet findByCoverName(String colName) throws SQLException {
        ResultSet rs = null;
        PreparedStatement stmt;
        try {
            String sql = "SELECT * FROM ebooks WHERE cover_name =?";
            stmt = connect.prepareStatement(sql);
            stmt.setString(1, colName);
            rs = stmt.executeQuery();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return rs;
    }

    /**
     * trova un record nel database per Status del file
     *
     * @param status
     * @return
     * @throws SQLException
     */
    public ResultSet findByStatus(Integer status) throws SQLException {
        ResultSet rs = null;
        try {
            Statement stmt;
            stmt = connect.createStatement();
            rs = stmt.executeQuery("SELECT * FROM ebooks WHERE status='" + status + "' order by id desc;");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return rs;
    }

    /**
     * modifica lo staus nel database e lo segna come completato
     *
     * @param Status
     * @param id
     * @throws java.sql.SQLException
     */
    public void setStatus(Integer Status, String id) throws SQLException {
        try {
            Statement stmt;
            stmt = connect.createStatement();
            String sql = "UPDATE ebooks SET status='" + Status + "' WHERE ID='" + id + "';";
            int affectedRows = stmt.executeUpdate(sql);
            if (affectedRows == 0) {
                throw new SQLException("Update failed");
            }

        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
    }

    public void setSync(String id, int sync) throws SQLException {
        try {
            Statement stmt;
            stmt = connect.createStatement();
            String sql = "UPDATE ebooks SET is_sync='" + sync + "' WHERE ID='" + id + "';";
            int affectedRows = stmt.executeUpdate(sql);
            stmt.close();
            if (affectedRows == 0) {
                throw new SQLException("Update failed");
            }

        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
    }

    /**
     * chiudo la connessione
     *
     * @throws java.sql.SQLException
     */
    public void closeConnection() throws SQLException {
        try {
            connect.close();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
    }

    /**
     * seleziona la riga del database con uno specifico nome del sito
     *
     * @param nameSite
     * @return
     * @throws java.sql.SQLException
     */
    public ResultSet findRowDatabase(String nameSite) throws SQLException {
        ResultSet rs = null;
        try {
            Statement stmt;
            stmt = connect.createStatement();
            rs = stmt.executeQuery("SELECT * FROM ebooks WHERE site_name='" + nameSite + "';");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return rs;
    }

    public ResultSet findBySitenameAndStatus(String nameSite, Integer status) throws SQLException {
        ResultSet rs = null;

        try {
            Statement stmt;
            stmt = connect.createStatement();
            rs = stmt.executeQuery("SELECT * FROM ebooks WHERE site_name='" + nameSite + "' AND status='" + status + "' order by id desc;");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return rs;
    }

    /**
     * controlla il resultSet e restituisce vero se trova elemento
     *
     * @param rs
     * @return
     * @throws java.sql.SQLException
     */
    public boolean isNext(ResultSet rs) throws SQLException {
        try {
            if (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return false;
    }

    public String getTitle(ResultSet rs) throws SQLException {
        String link = null;
        try {
            link = rs.getString("ebook_title");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return link;
    }

    /**
     * inserisce il nome del file nel database
     *
     * @param nameFile
     * @param id
     * @throws java.sql.SQLException
     */
    public void setFilename(String nameFile, String id) throws SQLException {
        try {
            Statement stmt;
            stmt = connect.createStatement();
            String sql = "UPDATE ebooks SET filename='" + nameFile + "' WHERE ID='" + id + "';";
            int affectedRows = stmt.executeUpdate(sql);
            if (affectedRows == 0) {
                throw new SQLException("Update failed");
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
    }

    public ResultSet findAllRecordsFirebase() throws SQLException {
        ResultSet rs = null;
        try {
            Statement stmt;
            stmt = connect.createStatement();
            rs = stmt.executeQuery("SELECT * FROM ebooks WHERE status=20 AND is_sync=0");
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return rs;
    }
}
