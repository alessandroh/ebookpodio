/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.utility.tool;


import java.io.File;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author alex
 */
public class Tool {

    String downloadDir;
    String ebookDownloadDir;
    Logger log = Logger.getLogger(Tool.class);
    

    public Tool(HashMap generalParams) {
        this.ebookDownloadDir = generalParams.get("ebookDownloadDir").toString();
        this.downloadDir = generalParams.get("downloadDir").toString();
    }

    /**
     * Specificando un percorso, creo una cartella nel caso essa non esista dove
     * salvare le immagini delle copertine
     */
    public void createCoverDir() {
        boolean create;
        try {
            //preparo la cartella per il download delle cover
            create = new File(downloadDir).mkdir();
            if (create) {
                System.out.println("cartella creata in: " + downloadDir);
            } else {
                System.out.println("cartella esisistente - path:" + downloadDir);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Specificando un percorso, creo una cartella nel caso essa non esista dove
     * salvare gli ebook
     */
    public void createDownloadEbookDir() {
        boolean create;
        try {
            //preparo la cartella per il download degli ebook
            create = new File(ebookDownloadDir).mkdir();
            if (create) {
                System.out.println("cartella creata in: " + ebookDownloadDir);
            } else {
                System.out.println("cartella esisistente - path:" + ebookDownloadDir);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }
    /**
     * Crea un HashMap con i valori dell'array args
     *
     * @param args array di stringhe con gli args
     * @return HashMap di stringhe
     */
    public HashMap createHashArgs(String[] args) {
       // HashMap<String, String> argument = new HashMap<>();
        HashMap argument = new HashMap();
        for (String arg : args) {
            String cod = arg.split("-")[0];
            String val = arg.split("-")[1];
            argument.put(cod, val);
        }
        return argument;
    }   

}
