/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.omniteca.utility.varproperty;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author alex
 */
public class VarProperty {

    Logger log = Logger.getLogger(VarProperty.class);

    String path;

    public VarProperty() {

        this.path = System.getProperty("user.dir");
    }

    /**
     * carica in un Hashtable tutte le variabile del file config.properties
     *
     * @return
     * @throws java.io.FileNotFoundException
     */
    public HashMap loadGeneralParams() throws FileNotFoundException, IOException {
        HashMap params = new HashMap();
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream(path + "/src/main/java/downloader.config.properties");
            try {
                prop.load(input);
            } catch (IOException ex) {
                log.error(ex.getMessage());
                throw ex;
            }
        } catch (FileNotFoundException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        try {

            params.put("webDProp", prop.getProperty("webDProp"));
            params.put("webDPath", prop.getProperty("webDPath"));
            params.put("host", prop.getProperty("host"));
            params.put("user", prop.getProperty("user"));
            params.put("pass", prop.getProperty("pass"));
            params.put("downloadDir", prop.getProperty("downloadDir"));
            params.put("torHost", prop.getProperty("torHost"));
            params.put("torPort", prop.getProperty("torPort"));
            params.put("ebookDownloadDir", prop.getProperty("ebookDownloadDir"));
            params.put("pathProfile", prop.getProperty("pathProfile"));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return params;
    }

    public HashMap loadFreeBookParams() throws IOException {
        HashMap params = new HashMap();
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream(path + "/src/main/java/freeBookSpot.config.properties");
            try {
                prop.load(input);
            } catch (IOException ex) {
                log.error(ex.getMessage());
                throw ex;
            }
        } catch (FileNotFoundException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        try {
            params.put("address", prop.getProperty("address"));
            params.put("value", prop.getProperty("value"));
            params.put("dropId", prop.getProperty("dropId"));
            params.put("dropSelectPath", prop.getProperty("dropSelectPath"));
            params.put("srcElementPath", prop.getProperty("srcElementPath"));
            params.put("downloadLinkPath", prop.getProperty("downloadLinkPath"));
            params.put("nextPagePath", prop.getProperty("nextPagePath"));
            params.put("nrPage", prop.getProperty("nrPage"));
            params.put("srcImageId", prop.getProperty("srcImageId"));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return params;
    }

    public HashMap loadDailyUpParams() throws FileNotFoundException, IOException {
        HashMap params = new HashMap();
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream(path + "/src/main/java/dailyUp.config.properties");
            try {
                prop.load(input);
            } catch (IOException ex) {
                log.error(ex.getMessage());
                throw ex;
            }
        } catch (FileNotFoundException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        try {
            params.put("freeDownloadPath", prop.getProperty("freeDownloadPath"));
            params.put("delSign", prop.getProperty("delSign"));
            params.put("downButton", prop.getProperty("downButton"));
            params.put("startDownLink", prop.getProperty("startDownLink"));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return params;
    }

    public HashMap loadAvaxHomeParams() throws IOException {
        HashMap params = new HashMap();
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream(path + "/src/main/java/avaxHome.config.properties");
            try {
                prop.load(input);
            } catch (IOException ex) {
                log.error(ex.getMessage());
                throw ex;
            }
        } catch (FileNotFoundException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        try {
            params.put("address", prop.getProperty("address"));
            params.put("classDetail", prop.getProperty("classDetail"));
            params.put("classTitle", prop.getProperty("classTitle"));
            params.put("downPath", prop.getProperty("downPath"));
            params.put("classDownFile", prop.getProperty("classDownFile"));
            params.put("classCaptcha", prop.getProperty("classCaptcha"));
            params.put("classFullDetail", prop.getProperty("classFullDetail"));
            params.put("classNextPage", prop.getProperty("classNextPage"));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return params;
    }

    public HashMap loadIcerboxParams() throws IOException {
        HashMap params = new HashMap();
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream(path + "/src/main/java/icerbox.config.properties");
            try {
                prop.load(input);
            } catch (IOException ex) {
                log.error(ex.getMessage());
                throw ex;
            }
        } catch (FileNotFoundException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        try {
            params.put("findLoginPath", prop.getProperty("findLoginPath"));
            params.put("emailForm", prop.getProperty("emailForm"));
            params.put("passwordForm", prop.getProperty("passwordForm"));
            params.put("icerboxEmail", prop.getProperty("icerboxEmail"));
            params.put("icerboxPassw", prop.getProperty("icerboxPassw"));
            params.put("logButton", prop.getProperty("logButton"));
            params.put("downloadButton", prop.getProperty("downloadButton"));
            params.put("captchaIcerboxPath", prop.getProperty("captchaIcerboxPath"));
            params.put("badLinkPath", prop.getProperty("badLinkPath"));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return params;
    }

    public HashMap loadPodioParams() throws IOException {
        HashMap params = new HashMap();
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream(path + "/src/main/java/podio.config.properties");
            try {
                prop.load(input);
            } catch (IOException ex) {
                log.error(ex.getMessage());
                throw ex;
            }
        } catch (FileNotFoundException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        try {
            params.put("idClient", prop.getProperty("idClient"));
            params.put("secretKey", prop.getProperty("secretKey"));
            params.put("emailPodio", prop.getProperty("emailPodio"));
            params.put("passwPodio", prop.getProperty("passwPodio"));
            params.put("appId", prop.getProperty("appId"));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }
        return params;
    }
    

}
